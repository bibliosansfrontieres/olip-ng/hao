import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { configSchema } from './config/schema';
import { CpuModule } from './cpu/cpu.module';
import { BatteryModule } from './battery/battery.module';
import { MemoryModule } from './memory/memory.module';
import { StorageModule } from './storage/storage.module';
import { NetworkModule } from './network/network.module';
import { SystemModule } from './system/system.module';
import { LedsModule } from './leds/leds.module';
import { MiscModule } from './misc/misc.module';
import { AppController } from './app.controller';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, validationSchema: configSchema }),
    CpuModule,
    BatteryModule,
    MemoryModule,
    StorageModule,
    NetworkModule,
    SystemModule,
    LedsModule,
    MiscModule,
  ],
  controllers: [AppController],
})
export class AppModule {}
