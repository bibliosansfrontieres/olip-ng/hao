import { Test, TestingModule } from '@nestjs/testing';
import { BatteryController } from './battery.controller';
import { BatteryService } from './battery.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from '../test/mocks/config-service.mock';

describe('BatteryController', () => {
  let controller: BatteryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BatteryController],
      providers: [BatteryService, ConfigService],
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    controller = module.get<BatteryController>(BatteryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getHasBattery', () => {
    it('should be defined', async () => {
      expect(await controller.getHasBattery()).toBeDefined();
    });
  });

  describe('getPercentage', () => {
    it('should be defined', async () => {
      expect(await controller.getPercentage()).toBeDefined();
    });
  });

  describe('getIsCharging', () => {
    it('should be defined', async () => {
      expect(await controller.getIsCharging()).toBeDefined();
    });
  });
});
