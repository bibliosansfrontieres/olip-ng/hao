import { Test, TestingModule } from '@nestjs/testing';
import { BatteryService } from './battery.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from '../test/mocks/config-service.mock';
import { BatteryHasBatteryDto } from './dto/battery-has-battery.dto';
import { BatteryPercentageDto } from './dto/battery-percentage.dto';
import { BatteryIsChargingDto } from './dto/battery-is-charging.dto';

describe('BatteryService', () => {
  let service: BatteryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BatteryService, ConfigService],
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    service = module.get<BatteryService>(BatteryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getHasBattery', () => {
    it('should return BatteryHasBatteryDto', async () => {
      expect(await service.getHasBattery()).toBeInstanceOf(
        BatteryHasBatteryDto,
      );
    });
  });

  describe('getPercentage', () => {
    it('should return BatteryPercentageDto', async () => {
      expect(await service.getPercentage()).toBeInstanceOf(
        BatteryPercentageDto,
      );
    });
  });

  describe('getIsCharging', () => {
    it('should return BatteryIsChargingDto', async () => {
      expect(await service.getIsCharging()).toBeInstanceOf(
        BatteryIsChargingDto,
      );
    });
  });
});
