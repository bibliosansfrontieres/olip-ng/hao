import { ApiProperty } from '@nestjs/swagger';

export class BatteryPercentageDto {
  @ApiProperty({ example: 45 })
  percentage: number;

  constructor(percentage: number) {
    this.percentage = percentage;
  }
}
