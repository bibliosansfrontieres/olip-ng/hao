import { ApiProperty } from '@nestjs/swagger';

export class BatteryHasBatteryDto {
  @ApiProperty({ example: true })
  hasBattery: boolean;

  constructor(hasBattery: boolean) {
    this.hasBattery = hasBattery;
  }
}
