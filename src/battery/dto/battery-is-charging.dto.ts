import { ApiProperty } from '@nestjs/swagger';

export class BatteryIsChargingDto {
  @ApiProperty({ example: true })
  isCharging: boolean;

  constructor(isCharging: boolean) {
    this.isCharging = isCharging;
  }
}
