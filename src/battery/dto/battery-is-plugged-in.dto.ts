import { ApiProperty } from '@nestjs/swagger';

export class BatteryIsPluggedInDto {
  @ApiProperty({ example: true })
  isPluggedIn: boolean;

  constructor(isPluggedIn: boolean) {
    this.isPluggedIn = isPluggedIn;
  }
}
