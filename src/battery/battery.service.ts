import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { BatteryHasBatteryDto } from './dto/battery-has-battery.dto';
import { battery } from 'systeminformation';
import { BatteryTypes } from '../config/battery-types.enum';
import { BatteryPercentageDto } from './dto/battery-percentage.dto';
import { BatteryIsChargingDto } from './dto/battery-is-charging.dto';
import { BatteryIsPluggedInDto } from './dto/battery-is-plugged-in.dto';
import { PiSugarCommandsEnum } from '../utils/enums/pi-sugar-commands.enum';
import { piSugarSocket } from '../utils/pi-sugar-socket.util';

@Injectable()
export class BatteryService {
  constructor(private configService: ConfigService) {}

  async getHasBattery(): Promise<BatteryHasBatteryDto> {
    let hasBattery = false;

    switch (this.configService.get('BATTERY_TYPE')) {
      case BatteryTypes.PI_SUGAR_3:
        hasBattery = await this.getPiSugar3HasBattery();
        break;
      default:
        const batteryData = await battery();
        hasBattery = batteryData.hasBattery;
        break;
    }

    return new BatteryHasBatteryDto(hasBattery);
  }

  async getPercentage(): Promise<BatteryPercentageDto> {
    let percentage = 0;

    switch (this.configService.get('BATTERY_TYPE')) {
      case BatteryTypes.PI_SUGAR_3:
        percentage = await this.getPiSugar3Percentage();
        break;
      default:
        const batteryData = await battery();
        percentage = batteryData.percent;
        break;
    }

    return new BatteryPercentageDto(percentage);
  }

  async getIsCharging(): Promise<BatteryIsChargingDto> {
    let isCharging = false;

    switch (this.configService.get('BATTERY_TYPE')) {
      case BatteryTypes.PI_SUGAR_3:
        isCharging = await this.getPiSugar3IsCharging();
        break;
      default:
        const batteryData = await battery();
        isCharging = batteryData.isCharging;
        break;
    }

    return new BatteryIsChargingDto(isCharging);
  }

  async getIsPluggedIn(): Promise<BatteryIsPluggedInDto> {
    let isPluggedIn = false;

    switch (this.configService.get('BATTERY_TYPE')) {
      case BatteryTypes.PI_SUGAR_3:
        isPluggedIn = await this.getPiSugar3IsPluggedIn();
        break;
      default:
        const batteryData = await battery();
        isPluggedIn = batteryData.acConnected;
        break;
    }

    return new BatteryIsPluggedInDto(isPluggedIn);
  }

  async getPiSugar3HasBattery(): Promise<boolean> {
    const batteryResponse = await piSugarSocket(
      PiSugarCommandsEnum.GET_BATTERY,
      {
        host: this.configService.get('PI_SUGAR_3_HOST'),
        port: this.configService.get('PI_SUGAR_3_PORT'),
      },
    );
    if (!batteryResponse) return false;
    return true;
  }

  async getPiSugar3Percentage(): Promise<number> {
    const batteryResponse = await piSugarSocket(
      PiSugarCommandsEnum.GET_BATTERY,
      {
        host: this.configService.get('PI_SUGAR_3_HOST'),
        port: this.configService.get('PI_SUGAR_3_PORT'),
      },
    );
    if (!batteryResponse) return 0;
    return parseInt(batteryResponse);
  }

  async getPiSugar3IsCharging(): Promise<boolean> {
    const batteryChargingResponse = await piSugarSocket(
      PiSugarCommandsEnum.GET_BATTERY_CHARGING,
      {
        host: this.configService.get('PI_SUGAR_3_HOST'),
        port: this.configService.get('PI_SUGAR_3_PORT'),
      },
    );
    if (!batteryChargingResponse) return false;
    return batteryChargingResponse === 'true';
  }

  async getPiSugar3IsPluggedIn(): Promise<boolean> {
    const batteryPowerPlugged = await piSugarSocket(
      PiSugarCommandsEnum.GET_BATTERY_POWER_PLUGGED,
      {
        host: this.configService.get('PI_SUGAR_3_HOST'),
        port: this.configService.get('PI_SUGAR_3_PORT'),
      },
    );
    if (!batteryPowerPlugged) return false;
    return batteryPowerPlugged === 'true';
  }
}
