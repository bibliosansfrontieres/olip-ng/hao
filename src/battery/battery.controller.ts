import { Controller, Get } from '@nestjs/common';
import { BatteryService } from './battery.service';
import { BatteryPercentageDto } from './dto/battery-percentage.dto';
import { BatteryHasBatteryDto } from './dto/battery-has-battery.dto';
import { BatteryIsChargingDto } from './dto/battery-is-charging.dto';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BatteryIsPluggedInDto } from './dto/battery-is-plugged-in.dto';

@ApiTags('battery')
@Controller('battery')
export class BatteryController {
  constructor(private readonly batteryService: BatteryService) {}

  @Get('has-battery')
  @ApiOperation({ summary: 'Does the device have a battery' })
  @ApiOkResponse({ type: BatteryHasBatteryDto })
  getHasBattery(): Promise<BatteryHasBatteryDto> {
    return this.batteryService.getHasBattery();
  }

  @Get('percentage')
  @ApiOperation({ summary: 'Get battery percentage' })
  @ApiOkResponse({ type: BatteryPercentageDto })
  getPercentage(): Promise<BatteryPercentageDto> {
    return this.batteryService.getPercentage();
  }

  @Get('is-charging')
  @ApiOperation({ summary: 'Is the device charging' })
  @ApiOkResponse({ type: BatteryIsChargingDto })
  getIsCharging(): Promise<BatteryIsChargingDto> {
    return this.batteryService.getIsCharging();
  }

  @Get('is-plugged-in')
  @ApiOperation({ summary: 'Is the device plugged in' })
  @ApiOkResponse({ type: BatteryIsPluggedInDto })
  getIsPluggedIn(): Promise<BatteryIsPluggedInDto> {
    return this.batteryService.getIsPluggedIn();
  }
}
