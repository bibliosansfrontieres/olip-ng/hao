import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { fsSize } from 'systeminformation';
import { StoragePathInfosDto } from './dto/storage-path-infos.dto';
import { StorageDto } from './dto/storage.dto';

@Injectable()
export class StorageService {
  constructor(private configService: ConfigService) {}

  async getStorage(): Promise<StorageDto> {
    const storagePathInfosDtos = await this.getStoragePathInfos();
    let totalBytes = 0;
    let availableBytes = 0;
    for (const storagePathInfosDto of storagePathInfosDtos) {
      totalBytes += storagePathInfosDto.totalBytes;
      availableBytes += storagePathInfosDto.availableBytes;
    }
    return new StorageDto(totalBytes, availableBytes, storagePathInfosDtos);
  }

  getStoragePaths(): string[] {
    const envStoragePaths = this.configService.get('STORAGE_PATHS') || '';
    if (envStoragePaths === '') return [];
    return envStoragePaths.split(';');
  }

  async getStoragePathInfos(): Promise<StoragePathInfosDto[]> {
    let storagePathInfosDtos: StoragePathInfosDto[] = [];

    switch (this.configService.get('STORAGE_TYPE')) {
      default:
        storagePathInfosDtos = await this.defaultGetStoragePathInfos();
        break;
    }

    return storagePathInfosDtos;
  }

  async defaultGetStoragePathInfos(): Promise<StoragePathInfosDto[]> {
    const diskPaths = this.getStoragePaths();
    const storagePathInfosDtos = [];
    for (const diskPath of diskPaths) {
      {
        try {
          const diskData = await fsSize();
          const targetDisk = diskData.find((disk) => disk.mount === diskPath);
          if (targetDisk) {
            storagePathInfosDtos.push(
              new StoragePathInfosDto(
                diskPath,
                targetDisk.available,
                targetDisk.size,
              ),
            );
          }
        } catch (error) {
          console.error(error);
        }
      }
    }
    return storagePathInfosDtos;
  }
}
