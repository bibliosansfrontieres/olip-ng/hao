export class StoragePathInfosDto {
  path: string;
  availableBytes: number;
  totalBytes: number;

  constructor(path: string, availableBytes: number, totalBytes: number) {
    this.path = path;
    this.availableBytes = availableBytes;
    this.totalBytes = totalBytes;
  }
}
