import { ApiProperty } from '@nestjs/swagger';
import { StoragePathInfosDto } from './storage-path-infos.dto';

export class StorageDto {
  @ApiProperty({ example: 100 })
  availableBytes: number;

  @ApiProperty({ example: 500 })
  totalBytes: number;

  @ApiProperty({
    example: [{ path: '/', availableBytes: 100, totalBytes: 500 }],
  })
  paths: StoragePathInfosDto[];

  constructor(
    totalBytes: number,
    availableBytes: number,
    paths: StoragePathInfosDto[],
  ) {
    this.totalBytes = totalBytes;
    this.availableBytes = availableBytes;
    this.paths = paths;
  }
}
