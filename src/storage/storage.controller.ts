import { Controller, Get } from '@nestjs/common';
import { StorageService } from './storage.service';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { StorageDto } from './dto/storage.dto';

@ApiTags('storage')
@Controller('storage')
export class StorageController {
  constructor(private readonly storageService: StorageService) {}

  @Get()
  @ApiOperation({ summary: 'Get storage informations' })
  @ApiOkResponse({ type: StorageDto })
  getStorage() {
    return this.storageService.getStorage();
  }
}
