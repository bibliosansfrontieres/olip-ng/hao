import * as Joi from 'joi';
import { LedTypesEnum } from './led-types.enum';

export const configSchema = Joi.object({
  PORT: Joi.number().default(80),
  CPU_LOAD_TYPE: Joi.string().optional(),
  CPU_TEMPERATURE_TYPE: Joi.string().optional(),
  MISC_SERIAL_TYPE: Joi.string().optional(),
  BATTERY_TYPE: Joi.string().optional(),
  MEMORY_USAGE_TYPE: Joi.string().optional(),
  NETWORK_HOTSPOT: Joi.string().optional(),
  HOSTAPD_CONF_PATH: Joi.string().default('/etc/hostapd/hostapd.conf'),
  STORAGE_TYPE: Joi.string().optional(),
  STORAGE_PATHS: Joi.string().optional(),
  PI_SUGAR_3_PORT: Joi.number().default(8423),
  PI_SUGAR_3_HOST: Joi.string().default('127.0.0.1'),
  LEDS_TYPE: Joi.string().default(LedTypesEnum.RASPBERRY_PI),
});
