import { Controller, Get } from '@nestjs/common';
import { CpuService } from './cpu.service';
import { ApiTags, ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { CpuLoadDto } from './dto/cpu-load.dto';
import { CpuTemperatureDto } from './dto/cpu-temperature.dto';

@ApiTags('cpu')
@Controller('cpu')
export class CpuController {
  constructor(private readonly cpuService: CpuService) {}

  @Get('load')
  @ApiOperation({ summary: 'Get CPU load in %' })
  @ApiOkResponse({ type: CpuLoadDto })
  getLoad() {
    return this.cpuService.getLoad();
  }

  @Get('temperature')
  @ApiOperation({ summary: 'Get CPU temperature in °C' })
  @ApiOkResponse({ type: CpuTemperatureDto })
  getTemperature() {
    return this.cpuService.getTemperature();
  }
}
