import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { cpuTemperature, currentLoad } from 'systeminformation';
import { CpuTemperatureDto } from './dto/cpu-temperature.dto';
import { CpuLoadDto } from './dto/cpu-load.dto';

@Injectable()
export class CpuService {
  constructor(private configService: ConfigService) {}

  async getLoad(): Promise<CpuLoadDto> {
    let load = 0;

    switch (this.configService.get('CPU_LOAD_TYPE')) {
      default:
        const currentLoadData = await currentLoad();
        load = currentLoadData.currentLoad;
        break;
    }

    return new CpuLoadDto(load);
  }

  async getTemperature(): Promise<CpuTemperatureDto> {
    let temperature = 0;

    switch (this.configService.get('CPU_TEMPERATURE_TYPE')) {
      default:
        const cpuTemperatureData = await cpuTemperature();
        temperature = cpuTemperatureData.main;
        break;
    }

    return new CpuTemperatureDto(temperature);
  }
}
