import { Test, TestingModule } from '@nestjs/testing';
import { CpuService } from './cpu.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from '../test/mocks/config-service.mock';
import { CpuLoadDto } from './dto/cpu-load.dto';
import { CpuTemperatureDto } from './dto/cpu-temperature.dto';

describe('CpuService', () => {
  let service: CpuService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CpuService, ConfigService],
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    service = module.get<CpuService>(CpuService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getLoad', () => {
    it('should return a CpuLoadDto', async () => {
      expect(await service.getLoad()).toBeInstanceOf(CpuLoadDto);
    });
  });

  describe('getTemperature', () => {
    it('should return a CpuTemperatureDto', async () => {
      expect(await service.getTemperature()).toBeInstanceOf(CpuTemperatureDto);
    });
  });
});
