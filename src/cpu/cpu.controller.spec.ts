import { Test, TestingModule } from '@nestjs/testing';
import { CpuController } from './cpu.controller';
import { CpuService } from './cpu.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from '../test/mocks/config-service.mock';

describe('CpuController', () => {
  let controller: CpuController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CpuController],
      providers: [CpuService, ConfigService],
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    controller = module.get<CpuController>(CpuController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getLoad', () => {
    it('should be defined', async () => {
      expect(await controller.getLoad()).toBeDefined();
    });
  });

  describe('getTemperature', () => {
    it('should be defined', async () => {
      expect(await controller.getTemperature()).toBeDefined();
    });
  });
});
