import { ApiProperty } from '@nestjs/swagger';

export class CpuLoadDto {
  @ApiProperty({ example: 4.00254688462 })
  loadPurcentage: number;

  constructor(loadPurcentage: number) {
    this.loadPurcentage = loadPurcentage;
  }
}
