import { ApiProperty } from '@nestjs/swagger';

export class CpuTemperatureDto {
  @ApiProperty({ example: 42 })
  temperature: number;

  constructor(temperature: number) {
    this.temperature = temperature;
  }
}
