import { ApiProperty } from '@nestjs/swagger';

export class MiscSerialDto {
  @ApiProperty({ example: '0000000000000000' })
  serial: string | undefined;

  constructor(serial: string | undefined) {
    this.serial = serial;
  }
}
