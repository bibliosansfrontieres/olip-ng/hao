import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { exec } from 'child_process';
import { MiscSerialDto } from './dto/misc-serial.dto';

@Injectable()
export class MiscService {
  constructor(private configService: ConfigService) {}

  async getSerial(): Promise<MiscSerialDto> {
    let serial = undefined;

    switch (this.configService.get('MISC_SERIAL_TYPE')) {
      default:
        serial = await this.getRaspberryPiSerial();
        break;
    }

    return new MiscSerialDto(serial);
  }

  async getRaspberryPiSerial(): Promise<string | undefined> {
    const serial = await new Promise<string | undefined>((resolve, reject) => {
      exec(
        "cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2",
        (error, stdout, stderr) => {
          if (error) {
            console.error(`error: ${error.message}`);
            reject(undefined);
          }
          if (stderr) {
            console.error(`stderr: ${stderr}`);
            reject(undefined);
          }
          resolve(stdout);
        },
      );
    });
    if (serial) {
      return serial.trim();
    }
    return serial;
  }
}
