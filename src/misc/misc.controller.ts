import { Controller, Get } from '@nestjs/common';
import { MiscService } from './misc.service';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { MiscSerialDto } from './dto/misc-serial.dto';

@ApiTags('misc')
@Controller('misc')
export class MiscController {
  constructor(private readonly miscService: MiscService) {}

  @Get('serial')
  @ApiOperation({ summary: 'Get serial ID' })
  @ApiOkResponse({ type: MiscSerialDto })
  getSerial() {
    return this.miscService.getSerial();
  }
}
