import { Injectable } from '@nestjs/common';
import { SystemUuidDto } from './dto/system-uuid.dto';
import { exec } from 'child_process';

@Injectable()
export class SystemService {
  async getSystemUuid(): Promise<SystemUuidDto> {
    const systemUuid = await new Promise<string | undefined>(
      (resolve, reject) => {
        exec('dmidecode -s system-uuid', (error, stdout, stderr) => {
          if (error) {
            console.error(`error: ${error.message}`);
            reject(undefined);
          }
          if (stderr) {
            console.error(`stderr: ${stderr}`);
            reject(undefined);
          }
          resolve(stdout.trim());
        });
      },
    );
    return new SystemUuidDto(systemUuid);
  }
}
