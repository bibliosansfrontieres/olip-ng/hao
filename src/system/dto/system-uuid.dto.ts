import { ApiProperty } from '@nestjs/swagger';

export class SystemUuidDto {
  @ApiProperty({ example: '00000000-0000-0000-0000-000000000000' })
  uuid: string | undefined;

  constructor(uuid: string | undefined) {
    this.uuid = uuid;
  }
}
