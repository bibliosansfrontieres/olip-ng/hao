import { Controller, Get } from '@nestjs/common';
import { SystemService } from './system.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('system')
@Controller('system')
export class SystemController {
  constructor(private readonly systemService: SystemService) {}

  @Get('uuid')
  getSystemUuid() {
    return this.systemService.getSystemUuid();
  }
}
