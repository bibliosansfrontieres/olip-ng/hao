import { Controller, Get } from '@nestjs/common';
import { MemoryService } from './memory.service';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { MemoryUsageDto } from './dto/memory-usage.dto';

@ApiTags('memory')
@Controller('memory')
export class MemoryController {
  constructor(private readonly memoryService: MemoryService) {}

  @Get('usage')
  @ApiOperation({ summary: 'Get memory usage in %' })
  @ApiOkResponse({ type: MemoryUsageDto })
  getUsage() {
    return this.memoryService.getUsage();
  }
}
