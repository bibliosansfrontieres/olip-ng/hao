import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { mem } from 'systeminformation';
import { MemoryUsageDto } from './dto/memory-usage.dto';

@Injectable()
export class MemoryService {
  constructor(private configService: ConfigService) {}

  async getUsage() {
    let usage = 0;

    switch (this.configService.get('MEMORY_USAGE_TYPE')) {
      default:
        const memoryData = await mem();
        usage =
          ((memoryData.total - memoryData.available) / memoryData.total) * 100;
        break;
    }

    return new MemoryUsageDto(usage);
  }
}
