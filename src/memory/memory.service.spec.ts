import { Test, TestingModule } from '@nestjs/testing';
import { MemoryService } from './memory.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from '../test/mocks/config-service.mock';
import { MemoryUsageDto } from './dto/memory-usage.dto';

describe('MemoryService', () => {
  let service: MemoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MemoryService, ConfigService],
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    service = module.get<MemoryService>(MemoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getUsage', () => {
    it('should return MemoryUsageDto', async () => {
      expect(await service.getUsage()).toBeInstanceOf(MemoryUsageDto);
    });
  });
});
