import { ApiProperty } from '@nestjs/swagger';

export class MemoryUsageDto {
  @ApiProperty({ example: 45.0264 })
  usage: number;

  constructor(usage: number) {
    this.usage = usage;
  }
}
