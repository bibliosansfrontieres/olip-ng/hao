import { PiSugarCommandsEnum } from './enums/pi-sugar-commands.enum';
import { PiSugarCommand } from './interfaces/pi-sugar-command.interface';

export const piSugarCommands: { [key in PiSugarCommandsEnum]: PiSugarCommand } =
  {
    [PiSugarCommandsEnum.GET_BATTERY]: {
      response: 'battery',
    },
    [PiSugarCommandsEnum.GET_BATTERY_CHARGING]: {
      response: 'battery_charging',
    },
    [PiSugarCommandsEnum.GET_BATTERY_POWER_PLUGGED]: {
      response: 'battery_power_plugged',
    },
  };
