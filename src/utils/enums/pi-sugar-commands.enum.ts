export enum PiSugarCommandsEnum {
  GET_BATTERY = 'get battery',
  GET_BATTERY_CHARGING = 'get battery_charging',
  GET_BATTERY_POWER_PLUGGED = 'get battery_power_plugged',
}
