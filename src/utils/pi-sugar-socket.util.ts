import { Socket } from 'net';
import { PiSugarCommandsEnum } from './enums/pi-sugar-commands.enum';
import { piSugarCommands } from './pi-sugar-commands.util';

export async function piSugarSocket(
  command: PiSugarCommandsEnum,
  options?: {
    host?: string;
    port?: string | number;
  },
): Promise<string | undefined> {
  try {
    const piSugarCommand = piSugarCommands[command];
    const client = new Socket();
    // Connect to Pi Sugar 3 socket
    client.connect(
      parseInt(options?.port?.toString() || '8423'),
      options?.host || '127.0.0.1',
    );
    const percentage = await new Promise<string>((resolve, reject) => {
      // When receiving Pi Sugar 3 data
      client.on('data', (data: Buffer) => {
        // Transform Buffer received to string
        const dataString = data.toString();
        // If string received begins with "command:"
        if (dataString.startsWith(`${piSugarCommand.response}:`)) {
          // Remove "command:" from string and trim
          const splitCommandResponseString = dataString.split(
            `${piSugarCommand.response}: `,
          );
          const commandResponse = splitCommandResponseString[1].trim();
          // Resolve command response
          client.end();
          resolve(commandResponse);
        }
      });
      client.on('error', (e) => {
        client.end();
        reject(e);
      });
      // Sending command to PiSugar
      client.write(`${command}`);
    });
    return percentage;
  } catch (e) {
    // If we cannot connect to Pi Sugar or any other problems
    console.error(e);
    return undefined;
  }
}
