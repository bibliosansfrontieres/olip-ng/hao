import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { writeFileSync } from 'fs';

async function generateSwagger() {
  const app = await NestFactory.create(AppModule);
  const options = new DocumentBuilder()
    .setTitle('HAO')
    .setDescription('Hardware API for OLIP')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  const jsonOutput = JSON.stringify(document, null, 2);
  writeFileSync('./swagger.json', jsonOutput);

  await app.close();
}

generateSwagger();
