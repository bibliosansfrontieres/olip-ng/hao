import { Test, TestingModule } from '@nestjs/testing';
import { LedsService } from './leds.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from '../test/mocks/config-service.mock';

describe('LedsService', () => {
  let service: LedsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LedsService, ConfigService],
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    service = module.get<LedsService>(LedsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
