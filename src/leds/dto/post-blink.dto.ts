import { BlinkSpeedEnum } from '../enums/blink-speed.enum';

export class PostBlinkDto {
  duration: number;
  speed: BlinkSpeedEnum = BlinkSpeedEnum.NORMAL;
}
