import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { LedTypesEnum } from '../config/led-types.enum';
import { BlinkSpeedEnum } from './enums/blink-speed.enum';
import { PostBlinkDto } from './dto/post-blink.dto';
import { exec } from 'child_process';

@Injectable()
export class LedsService {
  constructor(private configService: ConfigService) {}
  async postBlink(postBlinkDto: PostBlinkDto): Promise<void> {
    switch (this.configService.get('LEDS_TYPE')) {
      case LedTypesEnum.RASPBERRY_PI:
        await this.raspberryPiBlink(postBlinkDto);
        break;
      default:
        break;
    }
  }

  private async raspberryPiBlink(postBlinkDto: PostBlinkDto): Promise<void> {
    let sleep = 1000;
    switch (postBlinkDto.speed) {
      case BlinkSpeedEnum.FAST:
        sleep = 250;
        break;
      case BlinkSpeedEnum.SLOW:
        sleep = 4000;
        break;
      default:
        sleep = 1000;
        break;
    }
    const halfSleep = sleep / 2;

    for (let i = 0; i < (postBlinkDto.duration * 1000) / sleep; i++) {
      exec('echo 0 > /sys/class/leds/PWR/brightness');
      await new Promise((resolve) => setTimeout(resolve, halfSleep));
      exec('echo 255 > /sys/class/leds/PWR/brightness');
      await new Promise((resolve) => setTimeout(resolve, halfSleep));
    }

    return;
  }
}
