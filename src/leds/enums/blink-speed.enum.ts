export enum BlinkSpeedEnum {
  FAST = 'FAST',
  NORMAL = 'NORMAL',
  SLOW = 'SLOW',
}
