import { Body, Controller, Post } from '@nestjs/common';
import { LedsService } from './leds.service';
import { PostBlinkDto } from './dto/post-blink.dto';

@Controller('leds')
export class LedsController {
  constructor(private readonly ledsService: LedsService) {}

  @Post('blink')
  postBlink(@Body() postBlinkDto: PostBlinkDto) {
    return this.ledsService.postBlink(postBlinkDto);
  }
}
