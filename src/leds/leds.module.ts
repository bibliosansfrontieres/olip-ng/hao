import { Module } from '@nestjs/common';
import { LedsService } from './leds.service';
import { LedsController } from './leds.controller';

@Module({
  controllers: [LedsController],
  providers: [LedsService],
})
export class LedsModule {}
