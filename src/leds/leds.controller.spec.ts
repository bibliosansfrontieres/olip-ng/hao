import { Test, TestingModule } from '@nestjs/testing';
import { LedsController } from './leds.controller';
import { LedsService } from './leds.service';
import { ConfigService } from '@nestjs/config';
import { configServiceMock } from '../test/mocks/config-service.mock';

describe('LedsController', () => {
  let controller: LedsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LedsController],
      providers: [LedsService, ConfigService],
    })
      .overrideProvider(ConfigService)
      .useValue(configServiceMock)
      .compile();

    controller = module.get<LedsController>(LedsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
