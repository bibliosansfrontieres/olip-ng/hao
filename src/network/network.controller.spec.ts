import { Test, TestingModule } from '@nestjs/testing';
import { NetworkController } from './network.controller';
import { NetworkService } from './network.service';
import { ConfigService } from '@nestjs/config';

describe('NetworkController', () => {
  let controller: NetworkController;
  let configService: ConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NetworkController],
      providers: [NetworkService, ConfigService],
    })
      .overrideProvider(ConfigService)
      .useValue(configService)
      .compile();

    controller = module.get<NetworkController>(NetworkController);
    configService = module.get<ConfigService>(ConfigService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getInternetConnection', () => {
    it('should be defined', () => {
      expect(controller.getInternetConnection).toBeDefined();
    });

    it('should have @Get("internet-connection") route', () => {
      const routeMetadata = Reflect.getMetadata(
        'path',
        controller.getInternetConnection,
      );
      expect(routeMetadata).toBe('internet-connection');
    });
  });
});
