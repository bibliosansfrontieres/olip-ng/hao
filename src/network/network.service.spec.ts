import { Test, TestingModule } from '@nestjs/testing';
import { NetworkService } from './network.service';
import { ConfigService } from '@nestjs/config';
import { NetworkInternetConnectionDto } from './dto/network-internet-connection.dto';
import * as fs from 'fs';

describe('NetworkService', () => {
  let service: NetworkService;
  let configService: ConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NetworkService, ConfigService],
    })
      .overrideProvider(ConfigService)
      .useValue(configService)
      .compile();

    service = module.get<NetworkService>(NetworkService);
    configService = module.get<ConfigService>(ConfigService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getInternetConnection', () => {
    it('should return NetworkInternetConnectionDto', async () => {
      expect(await service.getInternetConnection()).toBeInstanceOf(
        NetworkInternetConnectionDto,
      );
    });

    it('should return false if internet flag file does not exist', async () => {
      jest.spyOn(fs, 'existsSync').mockReturnValueOnce(false);
      const result = await service.getInternetConnection();
      expect(result.isConnected).toEqual(false);
    });

    it('should return true if internet flag file exist', async () => {
      jest.spyOn(fs, 'existsSync').mockReturnValueOnce(true);
      const result = await service.getInternetConnection();
      expect(result.isConnected).toEqual(true);
    });
  });
});
