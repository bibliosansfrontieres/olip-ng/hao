export class NetworkHotspotSsidDto {
  ssid: string | null;

  constructor(ssid: string | null) {
    this.ssid = ssid;
  }
}
