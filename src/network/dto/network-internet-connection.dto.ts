export class NetworkInternetConnectionDto {
  isConnected: boolean;

  constructor(isConnected: boolean) {
    this.isConnected = isConnected;
  }
}
