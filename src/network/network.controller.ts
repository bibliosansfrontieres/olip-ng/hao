import { Controller, Get } from '@nestjs/common';
import { NetworkService } from './network.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('network')
@Controller('network')
export class NetworkController {
  constructor(private readonly networkService: NetworkService) {}

  @Get('hotspot-ssid')
  getHotspotSsid() {
    return this.networkService.getHotspotSsid();
  }

  @Get('internet-connection')
  getInternetConnection() {
    return this.networkService.getInternetConnection();
  }
}
