import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { existsSync, readFileSync } from 'fs';
import { NetworkHotspotSsidDto } from './dto/network-hotspot-ssid.dto';
import { NetworkInternetConnectionDto } from './dto/network-internet-connection.dto';
import axios from 'axios';
import * as EventEmitter from 'events';

@Injectable()
export class NetworkService {
  constructor(private configService: ConfigService) {}

  async getHotspotSsid(): Promise<NetworkHotspotSsidDto> {
    let ssid = null;

    switch (this.configService.get('NETWORK_HOTSPOT')) {
      default:
        if (existsSync('/olip-files/deploy/ssid')) {
          ssid = readFileSync('/olip-files/deploy/ssid').toString();
        } else {
          ssid = this.buildSsid();
        }
        break;
    }

    return new NetworkHotspotSsidDto(ssid);
  }

  buildSsid(): string {
    const ssidPrefix = this.getSsidPrefix();
    const id = this.getDeployId();
    const macId = this.getMacId();

    return `${ssidPrefix}${id}${macId}`;
  }

  getSsidPrefix(): string {
    if (existsSync('/olip-files/deploy/ssid-prefix')) {
      return readFileSync('/olip-files/deploy/ssid-prefix').toString();
    }
    return 'ideascube';
  }

  getDeployId(): string {
    if (existsSync('/olip-files/deploy/id')) {
      return '-' + readFileSync('/olip-files/deploy/id').toString().slice(-4);
    } else {
      return '';
    }
  }

  getMacId(): string {
    if (existsSync('/olip-files/network/mac-id')) {
      return '-' + readFileSync('/olip-files/network/mac-id').toString();
    } else {
      return '';
    }
  }

  async getInternetConnection(): Promise<NetworkInternetConnectionDto> {
    const internetPath = '/olip-files/network/internet';
    const isConnected = existsSync(internetPath);
    return new NetworkInternetConnectionDto(isConnected);
  }

  async checkUrl(
    eventEmitter: EventEmitter,
    url: string,
    timeout: number = 5000,
  ) {
    try {
      console.log(url);
      await axios.get(url, { timeout });
      eventEmitter.emit('connected');
      return true;
    } catch {
      return false;
    }
  }
}
