# Hardware API for OLIP

## [API Documentation](https://hao-bibliosansfrontieres-olip-3f78350ec78c3ab2cdde63b159fbde8aa.gitlab.io/)

## How to add type specific function

All the workflow example will be for a new CPU type.

For our example, a CPU temperature could not be retrieved with default function.

### Add new CPU temperature type

Our new CPU type is called RPI1.2

In `src/config/cpu-temperature-types.ts` you need to add :

```
export enum CpuTemperatureTypes {
  'RPI_1_2' = 'RPI_1_2',
}
```

Adding this in enum types, make users able to use environment variable CPU_TEMPERATURE_TYPE=RPI_1_2.

### Create new CPU temperature function

In `src/cpu/cpu.service.ts`, you can add at the end of the CpuService class a new function, for example : `getTemperatureRpi1_2`.

CPU temperature result expect a number, so you can write everything you want in this new function as it return a number.

After your new function is created, you need to add it in the switch that will select the function to use by CPU type.

In the `getTemperature()` function, there is a switch that use `CPU_TEMPERATURE_TYPE` environment variable value.

You can add a new switch case just above `default:` case.

```
switch (this.configService.get('CPU_TEMPERATURE_TYPE')) {
  case CpuTemperatureTypes.RPI_1_2:
    temperature = await this.getTemperatureRpi1_2();
  default:
    const cpuTemperatureData = await cpuTemperature();
    temperature = cpuTemperatureData.main;
    break;
}
```

You are now able to use the new function.
